from django.contrib import admin
from .models import Profile, About, Friends, Post, Like, Comment, FriendRecommendation, Message, Notification, Friendship, Follower, Following, Mention

# Register your models here.
admin.site.register(Profile)
admin.site.register(About)
admin.site.register(Friends)
admin.site.register(Post)
admin.site.register(Like)
admin.site.register(Comment)
admin.site.register(FriendRecommendation)
admin.site.register(Message)
admin.site.register(Notification)
admin.site.register(Friendship)
admin.site.register(Follower)
admin.site.register(Following)
admin.site.register(Mention)
