# Generated by Django 5.0.3 on 2024-04-15 05:45

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('socialmedia', '0011_follow'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='mentions',
            field=models.ManyToManyField(blank=True, related_name='mentioned_in_posts', to=settings.AUTH_USER_MODEL),
        ),
    ]
